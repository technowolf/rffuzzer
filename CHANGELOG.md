
## v1.2.1

- Fixed a critical bug where it was causing OS memory exhaustion

## v1.2.0

- Added option for specifying a HTTP Proxy
- Added a custom timeout functionality
- Fixed an issue with poller quiting too early.

## v1.1.0

- Added built in client and polling for interact.sh to provide with a OOB listener on the go
- Added option to test single URL
- Added option to specify output file to store interact.sh logs

## v1.0.0

- Initial release
