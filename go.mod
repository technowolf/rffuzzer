module gitlab.com/technowolf/rffuzzer

go 1.15

require (
	github.com/fatih/color v1.12.0
	github.com/projectdiscovery/gologger v1.1.4
	github.com/projectdiscovery/interactsh v0.0.3
)
