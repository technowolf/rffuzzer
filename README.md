# rffuzzer

rffuzzer is a simple SSRF Fuzzer to detect SSRF Injection via HTTP Headers

# Installation
```bash
go get -u gitlab.com/technowolf/rffuzzer
```

# Usage

| Flag | Usage |
| --- | --- |
| -uL PATH | File path for URLs to test.  |
| -u URL | A single url to test for SSRF |
| -s SERVER | Server to be used for checking SSRF |
| -p PROXY_URL | Specify a HTTP proxy for requests to be made |
| -t Timeout | Specify a HTTP request timeout. (default 20 seconds) |
| -o PATH | Path to store SSRF Requests and Responses (JSON) |
# TODO
- Multi Threading
- Selective header injections